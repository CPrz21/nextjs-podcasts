module.exports = {
  distDir: "build",
  webpack: (config) => {
    config.node = {
      fs: "empty",
    };
    return config;
  },
  // exportPathMap: async function (
  //   defaultPathMap,
  //   { dev, dir, outDir, distDir, buildId }
  // ) {
  //   return {
  //     "/": { page: "/" },
  //     "/channel": { page: "/channel" },
  //     // "/channel/:id": { page: "/channel", query: { id: { id } } },
  //   };
  // },
};
