import "isomorphic-fetch";
import Error from "next/error";
import Layout from "../components/Layout";
import ChannelGrid from "../components/ChannelGrid";

export async function getServerSideProps({ res }) {
  try {
    let req = await fetch("https://api.audioboom.com/channels/recommended");
    let { body: channels } = await req.json();

    return { props: { channels, statusCode: 200 } };
  } catch (error) {
    res.statusCode = 503;
    return { props: { channels: null, statusCode: 503 } };
  }
}

const HomePage = ({ channels, statusCode }) => {
  if (statusCode !== 200) {
    return <Error statusCode={statusCode} />;
  }

  return (
    <Layout title="Podcasts">
      <ChannelGrid channels={channels} />
    </Layout>
  );
};

export default HomePage;
